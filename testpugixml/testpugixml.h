#pragma once

#include <QtWidgets/QMainWindow>
#include "ui_testpugixml.h"

class testpugixml : public QMainWindow
{
    Q_OBJECT

public:
    testpugixml(QWidget *parent = nullptr);
    ~testpugixml();
protected:
    void OnInitDialog();
private:
    Ui::testpugixmlClass ui;
};
